package hu.skillversum.rssdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(value = "rss")
public class RssdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RssdemoApplication.class, args);
    }
}
