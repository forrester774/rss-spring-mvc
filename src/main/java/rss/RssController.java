package rss;

import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Description;
import com.rometools.rome.feed.rss.Item;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Scope("request")
@RestController
public class RssController {

    private List<Item> itemsList = new ArrayList<>();


    @GetMapping(path = "/my-channel")
    public Channel rss() {


        Channel channel = new Channel();
        channel.setFeedType("rss_2.0");
        channel.setTitle("SkillVersum Tutorial RSS Feed");
        channel.setDescription("Read about our newest Posts and Notes");
        channel.setLink("https://skillversum.com");
        Date channelDate = new Date();
        channel.setPubDate(channelDate);

        Item item = new Item();
        item.setAuthor("SkillVersum");
        item.setLink("placeholder majd a saját jegyzet link kerül ide");
        item.setTitle("RSS Channel Tutorial on SkillVersum");

        Description description = new Description();
        description.setValue("Check out this cool note on creating RSS channels.");
        item.setDescription(description);
        Date publishedDate = new Date();
        item.setPubDate(publishedDate);
        itemsList.add(item);


        item = new Item();
        item.setAuthor("SkillVersum");
        item.setLink("https://www.skillversum.com/note/view/13267ab7cb50a6a903fb26faaefafbf47cd623e4");
        item.setTitle("JSF Tutorial on SkillVersum");

        description = new Description();
        description.setValue("First part of JSF Notes series.");
        item.setDescription(description);
        publishedDate = new Date();
        item.setPubDate(publishedDate);
        itemsList.add(item);


        channel.setItems(itemsList);

        return channel;
    }
}